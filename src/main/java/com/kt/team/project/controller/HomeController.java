package com.kt.team.project.controller;

import com.kt.team.project.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = "/home")
    public String home(Model model) {
        User user = new User("audwlsd","dks7782","안명진");
        model.addAttribute("user", user);
        return "home";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

}
